﻿
insert into Company values 
			('Example Company 1'),
			('Example Company 2'),
			('Example Company 3'),
			('Example Company 4');

insert into Site Values
		('Richmond','Access to this site is restricted to company working hours'),
		('Kingston',null),
		('Twickenham',null),
		('Kew', '24 hour access available'),
		('Chiswick',null),
		('Brentford','Beware of the dog!'),
		('Birmingham',null);

insert into Companysite values
		(1,1,'Yes'),
		(1,3,'No'),
		(2,2,'Yes'),
		(3,4,'No'),
		(3,5,'Yes'),
		(4,6,'Yes'),
		(4,7,'No')

insert into CompanyDetail Values
	(1,'0121 2254 3698','2016-03-01','2017-03-01'),
	(1,'0121 2560 2569','2017-06-15','2018-06-15'),
	(1,'0121 5896 5896','2015-09-03','2016-09-03'),
	(2,'0121 5478 5689','2017-07-08','2018-07-08'),
	(3,'0208 1324 5879','2016-08-05','2017-08-05'),
	(4,'0208 2458 8965','2017-08-08','2018-08-08'),
	(4,'0208 2458 8989','2016-12-12','2017-12-12'),
	(5,'0121 4578 2563','2017-03-06','2018-03-06'),
	(6,'0208 5689 2458','2014-03-01','2016-03-01'),
	(7,'0121 2568 2352','2016-08-05','2017-08-05')

	