﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using CompanySiteApp.Models;
using System.Data;
using log4net;

namespace CompanySiteApp.DataAccess
{
   public class CompanySiteDetailsDAL
    {
        private static readonly ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        SqlConnection connection;
        public SqlConnection getConnection()
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["CompanySiteConnectionString"].ConnectionString;
            return new SqlConnection(connectionString);
        }
        public List<Company> getAllCompany()
        {
            List<Company> companyList = new List<Company>();
            try
            {
                connection = getConnection();
                string queryString = "select CompanyID, Name from Company";
                var command = new SqlCommand(queryString, connection);
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Company company = new Company();
                        company.CompanyID = (int)reader["CompanyID"];
                        company.CompanyName = reader["Name"].ToString();
                        companyList.Add(company);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in getAllCompany",ex);  
            }
            return companyList;
        }
        public List<Site> getAllSite()
        {
            List<Site> siteList = new List<Site>();
            try
            {
                connection = getConnection();
                string queryString = "select SiteID, SiteName from Site";
                var command = new SqlCommand(queryString, connection);
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Site site = new Site();
                        site.SiteID = (int)reader["SiteID"];
                        site.SiteName = reader["SiteName"].ToString();
                        siteList.Add(site);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in getAllSite", ex);
            }
            return siteList;
        }

        public int getCompanySiteIfExists(int companyID, int siteID)
        {
            int companySiteID =0;
            try
            {
                connection = getConnection();
                string queryString = "select CompanySiteID from CompanySite where CompanyID=@companyID and SiteID =@siteID";
                var command = new SqlCommand(queryString, connection);
                command.Parameters.Add("@companyID", SqlDbType.Int);
                command.Parameters["@companyID"].Value = companyID;
                command.Parameters.Add("@siteID", SqlDbType.Int);
                command.Parameters["@siteID"].Value = siteID;
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        companySiteID =  (int)reader["CompanySiteID"];
                         
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in getCompanySiteIfExists", ex);

            }
            connection.Close();
            return companySiteID;
        }
        public List<CompanySiteDetails> getCompanySiteDetails()
        {
            List<CompanySiteDetails> companySiteDetailList = new List<CompanySiteDetails>();
            try
            {
                connection = getConnection();
                string queryString = "Exec dbo.usp_get_CompanySiteDetails";
                var command = new SqlCommand(queryString, connection);
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        CompanySiteDetails companySiteDetail = new CompanySiteDetails();
                        companySiteDetail.CompanyDetailID = (int)reader["CompanyDetailID"];
                        companySiteDetail.CompanySiteID = (int)reader["CompanySiteID"];
                        companySiteDetail.CompanyID = (int)reader["CompanyID"];
                        companySiteDetail.CompanyName = reader["Name"].ToString();
                        companySiteDetail.SiteID = (int)reader["SiteID"];
                        companySiteDetail.SiteName = reader["SiteName"].ToString();
                        companySiteDetail.IsMainSite = reader["IsMainSite"].ToString();
                        companySiteDetail.Notes = reader["Notes"].ToString();
                        companySiteDetail.PhoneNumber = reader["PhoneNumber"].ToString();
                        companySiteDetail.StartDate = (DateTime)reader["StartDate"];
                        companySiteDetail.EndDate = (DateTime)reader["EndDate"];
                        companySiteDetailList.Add(companySiteDetail);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in getCompanySiteDetails", ex);

            }
            return companySiteDetailList;
        }
        public CompanySiteDetails getCompanySiteDetailByKey(int CompanyDetailID)
        {
            CompanySiteDetails companySiteDetail=null;
           try
            {
                connection = getConnection();
                string queryString = "Exec dbo.usp_get_CompanySiteDetails";
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@companyDetailID", CompanyDetailID);

                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        companySiteDetail = new CompanySiteDetails();
                        companySiteDetail.CompanyDetailID = (int)reader["CompanyDetailID"];
                        companySiteDetail.CompanySiteID = (int)reader["CompanySiteID"];
                        companySiteDetail.CompanyID = (int)reader["CompanyID"];
                        companySiteDetail.CompanyName = reader["Name"].ToString();
                        companySiteDetail.SiteID = (int)reader["SiteID"];
                        companySiteDetail.SiteName = reader["SiteName"].ToString();
                        companySiteDetail.IsMainSite = reader["IsMainSite"].ToString();
                        companySiteDetail.Notes = reader["Notes"].ToString();
                        companySiteDetail.PhoneNumber = reader["PhoneNumber"].ToString();
                        companySiteDetail.StartDate = (DateTime)reader["StartDate"];
                        companySiteDetail.EndDate = (DateTime)reader["EndDate"];
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in getCompanySiteDetailByKey", ex);

            }
            return companySiteDetail;
        }
        public Result addCompany(Company company)
        {
            Result result = new Result();
            try
            {
                connection = getConnection();
                string queryString = "INSERT INTO Company VALUES  (@CompanyName) ";
                var command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@CompanyName", company.CompanyName);
                connection.Open();
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
                result.ResultID = 0;
                result.ResultMessage = "Record Inserted Successfully";
            }
            catch (Exception ex)
            {
                log.Error("Error in addCompany", ex);
                result.ResultID = 1;
                result.ResultMessage = "Error Inserting Record. Error - {ex.message}";
            }
            return result;
        }
        public Result addSite(Site site)
        {
            Result result = new Result();
            try
            {
                connection = getConnection();
                string queryString = "INSERT INTO Site VALUES  (@SiteName,@Notes) ";
                var command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@SiteName", site.SiteName);
                command.Parameters.AddWithValue("@Notes", site.Notes);
                connection.Open();
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
                result.ResultID = 0;
                result.ResultMessage = "Record Inserted Successfully";
            }
            catch (Exception ex)
            {
                log.Error("Error in addSite", ex);
                result.ResultID = 1;
                result.ResultMessage = "Error Inserting Record. Error - {ex.message}";
            }
            return result;
        }
        public Result addCompanySiteDetail(CompanySiteDetails companySiteDetail)
        {
            Result result = new Result();
            try
            {
                connection = getConnection();
                int added = 0;
                int companySiteID = getCompanySiteIfExists(Int32.Parse(companySiteDetail.CompanyName), Int32.Parse(companySiteDetail.SiteName));
                if (companySiteID == 0)
                {
                    string queryString = "INSERT INTO CompanySite VALUES  (@CompanyID,@SiteID,@IsMainSite) ";
                    var command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue("@CompanyID", Int32.Parse(companySiteDetail.CompanyName));
                    command.Parameters.AddWithValue("@SiteID", Int32.Parse(companySiteDetail.SiteName));
                    command.Parameters.AddWithValue("@IsMainSite", companySiteDetail.IsMainSite);
                    connection.Open();
                    command.CommandType = CommandType.Text;
                    added = command.ExecuteNonQuery();
                    connection.Close();
                    companySiteID = getCompanySiteIfExists(Int32.Parse(companySiteDetail.CompanyName), Int32.Parse(companySiteDetail.SiteName));
                }
                if (companySiteID!=0)
                {
                    string queryString = "INSERT INTO CompanyDetail VALUES  (@CompanySiteID,@PhoneNumber,@StartDate,@EndDate) ";
                    var command1 = new SqlCommand(queryString, connection);
                    command1.Parameters.AddWithValue("@CompanySiteID", companySiteID);
                    command1.Parameters.AddWithValue("@PhoneNumber", companySiteDetail.PhoneNumber);
                    command1.Parameters.AddWithValue("@StartDate", companySiteDetail.StartDate);
                    command1.Parameters.AddWithValue("@EndDate", companySiteDetail.EndDate);
                    connection.Open();
                    command1.ExecuteNonQuery();
                    connection.Close();
                    result.ResultID = 0;
                    result.ResultMessage = "Record Inserted Successfully";
                }
                else
                {
                    result.ResultID = 1;
                    result.ResultMessage = "Error Inserting Record. Company Site Key not. added";
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in addCompanySiteDetail", ex);
                result.ResultID = 1;
                result.ResultMessage = "Error Inserting Record. Error - {ex.message}";
            }
            return result;
        }
        public Result updateCompanySiteDetail(CompanySiteDetails companySiteDetail)
        {
            Result result = new Result();
            try
            {
                connection = getConnection();
                int added = 0;
                string queryString = "update CompanyDetail set PhoneNumber = @PhoneNumber,StartDate = @StartDate,EndDate=@EndDate where CompanyDetailID =@CompanyDetailID ";
                var command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@CompanyDetailID", companySiteDetail.CompanyDetailID);
                command.Parameters.AddWithValue("@PhoneNumber", companySiteDetail.PhoneNumber);
                command.Parameters.AddWithValue("@StartDate", companySiteDetail.StartDate);
                command.Parameters.AddWithValue("@EndDate", companySiteDetail.EndDate);
                connection.Open();
                command.CommandType = CommandType.Text;
                added = command.ExecuteNonQuery();
                if (added > 0)
                {
                    queryString = "update CompanySite set IsMainSite = @IsMainSite where CompanySiteID =@CompanySiteID ";
                    command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue("@CompanySiteID", companySiteDetail.CompanySiteID);
                    command.Parameters.AddWithValue("@IsMainSite", companySiteDetail.IsMainSite);
                    command.CommandType = CommandType.Text;
                    added = command.ExecuteNonQuery();

                }
                connection.Close();
                result.ResultID = 0;
                result.ResultMessage = "Record Updated Successfully";
            }
            catch (Exception ex)
            {
                log.Error("Error in addCompanySiteDetail", ex);
                result.ResultID = 1;
                result.ResultMessage = "Error Updating Record. Error - {ex.message}";
            }
            return result;
        }

    }
}