﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CompanySiteApp.Models
{
    public class CompanySiteDetails
    {
        [Required]
        public int CompanyDetailID { get; set; }
        [Required]
        public int CompanySiteID { get; set; }
        [Required]
        public int CompanyID {get;set;}
        [Required]
        public string CompanyName {get;set;}
        [Required]
        public int SiteID {get;set;}
        [Required]
        public string SiteName { get; set; }
        [Required]
        public string IsMainSite {get;set;}
        public string Notes {get;set;}
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime StartDate { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime EndDate { get; set; }
    }
}