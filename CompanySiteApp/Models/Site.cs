﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CompanySiteApp.Models
{
    public class Site
    {
        //Primary Key
        [Key]
        public int SiteID { get; set; }
        [Required]
        public string SiteName { get; set; }
        public string Notes { get; set; }
    }
}