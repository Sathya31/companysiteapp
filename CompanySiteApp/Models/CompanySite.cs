﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CompanySiteApp.Models
{
    public class CompanySite
    {
        [ForeignKey("Company")]
        [Required]
        public int CompanyID { get; set; }
        [ForeignKey("Site")]
        [Required]
        public int SiteID { get; set; }
        [Required]
        public bool IsMainSite { get; set; }
    }
}