﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CompanySiteApp.Models
{
    public class Company
    {
        //Primary Key
        [Key]
        public int CompanyID { get; set; }
        [Required]
        public string CompanyName { get; set; }
    }
}