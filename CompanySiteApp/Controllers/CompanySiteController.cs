﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CompanySiteApp.DataAccess;
using CompanySiteApp.Models;

namespace CompanySiteApp.Controllers
{
    public class CompanySiteController : Controller
    {
        // GET: CompanySite
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CompanySiteDetails()
        {
            CompanySiteDetailsDAL compSiteDetails = new CompanySiteDetailsDAL();
            return View(compSiteDetails.getCompanySiteDetails());
        }

        public ActionResult CreateCompany()
        {
            ViewBag.Error = "";
            ViewBag.Result = "";
            return View();
        }
        [HttpPost]
        public ActionResult CreateCompany(Company company)
        {
            ViewBag.Error = "";
            ViewBag.Result = "";
            if (ModelState.IsValid)
                {
                    CompanySiteDetailsDAL compSiteDetails = new CompanySiteDetailsDAL();
                    Result processMessage = compSiteDetails.addCompany(company);
                    if (processMessage.ResultID == 0)
                         ViewBag.Result = processMessage.ResultMessage;
                    else ViewBag.Error = processMessage.ResultMessage;
                }
            return View();
        }
        public ActionResult CreateSite()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateSite(Site site)
        {
            ViewBag.Error = "";
            ViewBag.Result = "";
            if (ModelState.IsValid)
            {
                CompanySiteDetailsDAL compSiteDetails = new CompanySiteDetailsDAL();
                Result processMessage = compSiteDetails.addSite(site);
                if (processMessage.ResultID == 0)
                    ViewBag.Result = processMessage.ResultMessage;
                else ViewBag.Error = processMessage.ResultMessage;
            }
            return View();
        }
        public ActionResult ViewCompanySiteDetails(CompanySiteDetails companySiteDetail)
        {
            return View();
        }
        public ActionResult EditCompanyDetails(int companyDetailID)
        {
            CompanySiteDetailsDAL compSiteDetails = new CompanySiteDetailsDAL();
            CompanySiteDetails compDetails = compSiteDetails.getCompanySiteDetailByKey(companyDetailID);
            ViewBag.IsMainSiteList = new SelectList(new List<string> { "Yes", "No" }, compDetails.IsMainSite);
            return View(compDetails);
        }
        [HttpPost]
        public ActionResult CreateCompanySiteDetails(CompanySiteDetails companySiteDetail)
        {
            ViewBag.Error = "";
            ViewBag.Result = "";
            if (ModelState.IsValid)
            {
                CompanySiteDetailsDAL compSiteDetails = new CompanySiteDetailsDAL();
                Result processMessage = compSiteDetails.addCompanySiteDetail(companySiteDetail);
                if (processMessage.ResultID == 0)
                    ViewBag.Result = processMessage.ResultMessage;
                else ViewBag.Error = processMessage.ResultMessage;
                return RedirectToAction("ViewCompanySiteDetails", companySiteDetail);
            }
            else { ViewBag.Error = "Model State is Invalid"; };
            return View();
        }
        public ActionResult CreateCompanySiteDetails()
        {
            CompanySiteDetailsDAL compSiteDetails = new CompanySiteDetailsDAL();
            ViewBag.CompanyList = new SelectList(compSiteDetails.getAllCompany(),"CompanyID","CompanyName");
            ViewBag.SiteList = new SelectList(compSiteDetails.getAllSite(), "SiteID", "SiteName");
            ViewBag.IsMainSiteList = new SelectList ( new List<string> { "Yes", "No" },"IsMainSite" );
            return View();
        }
        [HttpPost]
        public ActionResult EditCompanyDetails(CompanySiteDetails companySiteDetail)
        {
            ViewBag.Error = "";
            ViewBag.Result = "";
            if (ModelState.IsValid)
            {
                CompanySiteDetailsDAL compSiteDetails = new CompanySiteDetailsDAL();
                Result processMessage = compSiteDetails.updateCompanySiteDetail(companySiteDetail);
                if (processMessage.ResultID == 0)
                    ViewBag.Result = processMessage.ResultMessage;
                else ViewBag.Error = processMessage.ResultMessage;
                return RedirectToAction("ViewCompanySiteDetails", companySiteDetail);
            }
            else { ViewBag.Error = "Model State is Invalid"; };
            return View();
        }
    }
}