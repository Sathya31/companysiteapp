﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CompanySiteApp;
using CompanySiteApp.Controllers;

namespace CompanySiteApp.Tests.Controllers
{
    [TestClass]
    public class CompanySiteControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            CompanySiteController controller = new CompanySiteController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
        
    }
}
